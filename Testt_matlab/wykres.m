load('wyniki1.mat')


figure('Name','Wynik Symulacji','NumberTitle','off')
subplot(2,1,1)
plot(y(1,:),y(2:3,:))
xlabel('t[h]')
ylabel('Q[m^3 /h]')

legend('Q_1','Q_2')

subplot(2,1,2)
plot(y(1,:),y(4,:))
xlabel('t[h]')
ylabel('Q[m^3...]')
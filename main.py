import numpy
import matplotlib.pyplot as plt
from matplotlib.collections import EventCollection
import matplotlib.gridspec

t0 = 0
# Masa powietrza [kg]
# 0,5*0,5*0,5 * 1,3
szerokosc = 1
wysokosc = 0.7
glebokosc = 0.5
mp = szerokosc * wysokosc * glebokosc * 1.3
# Ciepllow własciwe powietrza J/(K*kg)
cp = 2020
# Sprawność grałki %
n = 0.80
# Moc grzałki [W]
P = 20
# Temp. poczatkowa ->dalej obecna
T0 = 22
# Temperatura otoczenia
Tr = 20

F = 2 * wysokosc * szerokosc + 2 * wysokosc * glebokosc + 2 * szerokosc * glebokosc


# Grubość ścianki 0,5cm
# T -> obecna temperatura


def nagrzewanie(t, T):
    return T + P * n * t / (mp * cp)


def stygniecie(t, T, tmp):
    Q = T * cp * mp
    Q -= strata_Q(t, tmp)
    return Q / (cp * mp)


def sumator_odejmujacy(Qin1, Qin2):
    return Qin1 - Qin2


def strata_Q(t, dT):
    # Lambda = 0,009
    # Q = F * lambda * dT*t
    # lb szerokosc* U 1,2
    # lmb = 0.05*1.2
    U = 1.5
    return -F * U * dT


# skokowy przystost. MIerzyć czy po seknudize przekroczone czy nie, jeśli
# nie o jakąś tam wartość to bedzie czekał aż wystygnie o 2 st poniżej


def symulacja(tzadane, czas, dane_P, dane_S):
    T = T0
    dane_T = []
    # Jeśli grzałka pracuje za długo z mocą maksymalna, następuje powiadomienie
    over = 0
    save = False
    global P
    for i in range(czas):

        dane_T.append(T)
        dane_P.append(P)
        tmp1 = sumator_odejmujacy(Tr, T)
        tmp2 = sumator_odejmujacy(tzadane, T)
        if tmp2 > 0.1:
            T = nagrzewanie(1, T)
            tmp1 = sumator_odejmujacy(Tr, T)
            if tmp2 > 5 and P <= 240:
                P += 10
            elif tmp2 > 3 and P <= 243:
                P += 7
            elif tmp2 > 1 and P <= 245:
                P += 5
            elif tmp2 > 0.5 and P <= 247:
                P += 3


        else:
            if tmp2 < -0.1 and tmp2 > -0.5 and P > 5:
                P -= 5
            elif tmp2 > -1:
                P -= 1

        if P >= 250 and over <= 300:
            over += 1
        elif P < 250:
            over = 0
        elif over > 120:
            print("ERROR: HEATER OVERLOAD")
        if P < 20 and not save:
            save = not save
            print("Energy saving mode active")
        elif P >= 20:
            save = False
        tmp3 = T
        T = stygniecie(1, T, tmp1)
        dane_S.append(tmp3 - T)
    return dane_T


# [s]
# [C]
czas_dzialania = 150
T_zadane = 30

wyniki2 = []
wyniki3 = []
wyniki = symulacja(T_zadane, czas_dzialania, wyniki2, wyniki3)
czas = [i for i in range(czas_dzialania)]
fig = plt.figure()
gs = matplotlib.gridspec.GridSpec(2, 2)
ax = fig.add_subplot(gs[0, :])
bx = fig.add_subplot(gs[1, 1])
cx = fig.add_subplot(gs[1, 0])

ax.plot(czas, wyniki, 'r')
ax.set_title("Zmiana temperatury w czasie, T zadane: " + str(T_zadane) + "st C")
ax.set_ylabel("Temperatura [C]")
ax.set_xlabel("Czas[s]")

bx.plot(czas, wyniki2, 'b')
bx.set_title("Chwilowa moc grzałki")
bx.set_ylabel("Moc[W]")
bx.set_xlabel("Czas[s]")

cx.plot(czas, wyniki3, 'g')
cx.set_title("Straty temperatury w czasie")
cx.set_ylabel("Strata temp [C]")
cx.set_xlabel("Czas[s]")
fig.tight_layout()
plt.show()
